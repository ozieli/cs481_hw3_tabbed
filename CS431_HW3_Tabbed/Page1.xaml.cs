﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CS431_HW3_Tabbed
{
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        int babystatus = 0;

        async void BabyLeave(object sender, EventArgs e)
        {
            

            if (babystatus == 0)
            {
               await DisplayAlert("Are you sure?", "Are you going to leave a baby alone?", "Sure", "Nah");
                textheader.Text = "You left the baby alone. Awake. Wow.";
            }
            else
            {
                await DisplayAlert("Alert", "You put the baby to bed.", "Let it sleep");
                textheader.Text = "The baby is asleep in bed!";
            }

            await Navigation.PushAsync(new Page3());

        }


        async void BabyFeed(object sender, EventArgs e) //button to mark that the baby was fed.
        {
            babyimage.Source = "deepsleep.png";

            if (babystatus == 0)
                textheader.Text = "The baby is fed, and fell asleep in your arms.";
            else textheader.Text = "The baby is fed and fell asleep in bed.";

            await DisplayAlert("Alert", "You fed the baby!", "Let it sleep.");
        }

        public void BabyBed(object sender, EventArgs e) //button to mark that the baby was put to bed.
        {
            babystatus = 1;

        }
    }
}
